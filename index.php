<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S01 A1 PHP Basics and Selection Control Structures</title>
</head>
<body>
	<h2>Letter-Based-Grading</h2>
	<p>87 is equivalent to <?php echo getLetterGrade(87)?></p>
	<p>94 is equivalent to <?php echo getLetterGrade(94)?></p>
	<p>74 is equivalent to <?php echo getLetterGrade(74)?></p>

</body>
</html>