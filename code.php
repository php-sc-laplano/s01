<?php
function getLetterGrade($lettergrade){

	if($lettergrade >= 98 && $lettergrade <=100){
		return 'A+';
	}else if($lettergrade >= 95 && $lettergrade <= 97){
		return 'A';
	}else if($lettergrade >= 92 && $lettergrade <= 94){
		return 'A-';
	}else if($lettergrade >= 89 && $lettergrade <= 91){
		return 'B+';
	}else if($lettergrade >= 86 && $lettergrade <= 88){
		return 'B';
	}else if($lettergrade >= 83 && $lettergrade <= 85){
		return 'B-';
	}else if($lettergrade >= 80 && $lettergrade <= 82){
		return 'C+';
	}else if($lettergrade >= 77 && $lettergrade <= 79){
		return 'C';
	}else if($lettergrade >= 75 && $lettergrade <= 76){
		return 'C-';
	}else{
		return 'D';
	}

}

?>